/**
 * routes related to category.
 */
'use strict';

module.exports = function (app) {
    app.route('/api/v1/categories').get(app.ensureAuthenticated, app.getCategories)
        .post(app.ensureAuthenticated, app.createCategory);
    app.route('/api/v1/category/:categoryId').get(app.ensureAuthenticated, app.getCategoryById)
        .put(app.ensureAuthenticated, app.updateCategoryById)
        .delete(app.ensureAuthenticated, app.deleteCategoryById);
}