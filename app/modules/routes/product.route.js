/**
 * routes related to product.
 */
'use strict';
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function (app) {
    app.route('/api/v1/products').get(app.ensureAuthenticated, app.getProducts)
        .post(app.ensureAuthenticated, multipartMiddleware, app.createProduct);
    app.route('/api/v1/product/:productId').get(app.ensureAuthenticated, app.getProductById)
        .put(app.ensureAuthenticated,multipartMiddleware, app.updateProductById)
        .delete(app.ensureAuthenticated, app.deleteProductById);
    app.route('/api/v1/categories/product/:categoryId').get(app.ensureAuthenticated, app.getProductsByCategoryId);
    app.route('/api/v1/stats/data').get(app.ensureAuthenticated, app.getStatsData);


};