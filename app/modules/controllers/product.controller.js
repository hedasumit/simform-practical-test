"use strict";
var Category = require('../models/category.model.js').Category,
    Product = require('../models/product.model.js').Product,
    apiCodes = require('../../config/api-codes.js'),
    fs   = require('fs'),
    async = require('async');
module.exports = function (app) {


    app.getProductsByCategoryId = function (req, res) {
        if (req.params.categoryId) {
            Product.find({category: req.params.categoryId}).exec(function (err, products) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting products'});
                }
                else if (!products.length) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Products not Available'});
                } else {
                    return res.status(apiCodes.SUCCESS).send({
                        data: products
                    });
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Category id Not provided'
            });
        }
    };


    app.getProducts = function (req, res) {
        req.query.page = parseInt(req.query.page);
        req.query.page = req.query.page - 1;
        req.query.limit = parseInt(req.query.limit);
        async.parallel([
                function (callback) {
                    Product.count().exec(function (err, count) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, count);
                        }
                    });
                },
                function (callback) {
                    Product.find()
                        .limit(req.query.limit).skip(req.query.page * req.query.limit)
                        .exec(function (err, products) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, products);
                        }
                    });
                }
            ],
            function (err, result) {
                if (err) {
                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Products not Available'});
                }
                var responseObject = {
                    pagination: {count: result[0], limit: req.query.limit},
                    data: result[1]
                };
                res.json(responseObject);

            });
    };

    app.createProduct = function (req, res) {
        if (req.body.name) {
            Product.findOne({name: req.body.name}).exec(function (err, product) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Products not Found'});
                }
                else if (product) {
                    return res.status(apiCodes.DUPLICATE).send({message: 'Product Already Exit'});

                }
                else {
                    var newProduct = new Product();
                    newProduct.name = req.body.name;
                    newProduct.category = req.body.category;
                    newProduct.discount = req.body.discount;
                    newProduct.price = req.body.price;
                    newProduct.netPrice = req.body.netPrice;
                    newProduct.description = req.body.description;
                    if(req.files.image){
                        var bitmap = fs.readFileSync(req.files.image.path);
                        newProduct.image =  new Buffer(bitmap).toString('base64');
                    }
                    newProduct.save(function (err, data) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in creating product',err:err});
                        }
                        if (data) {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'Product Created'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({message: 'Name Required'});
        }
    };


    app.getProductById = function (req, res) {
        if (req.params.productId) {
            Product.findById(req.params.productId).exec(function (err, productObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting product'});
                }
                else if (!productObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Products not Available'});
                } else {
                    return res.status(apiCodes.SUCCESS).send({
                        data: productObject
                    });
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Product id Not provided'
            });
        }

    };


    app.updateProductById = function (req, res) {
        if (req.params.productId) {
            Product.findById(req.params.productId).exec(function (err, productObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting product'});
                }
                else if (!productObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Products not Available'});
                } else {
                    productObject.name = req.body.name;
                    productObject.category = req.body.category;
                    productObject.price = req.body.price;
                    productObject.discount = req.body.discount;
                    productObject.netPrice = req.body.netPrice;
                    productObject.description = req.body.description;
                    if(req.files.image){
                        var bitmap = fs.readFileSync(req.files.image.path);
                        productObject.image =  new Buffer(bitmap).toString('base64');
                    }
                    productObject.save(function (err, response) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({
                                message: 'Error in updating product'
                            });
                        } else {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'Product updated'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Product id Not provided'
            });
        }

    };

    app.deleteProductById = function (req, res) {
        if (req.params.productId) {
            Product.findById(req.params.productId).exec(function (err, productObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting product'});
                }
                else if (!productObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Products not Available'});
                } else {
                    productObject.remove(function (err, response) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({
                                message: 'Error in deleting product'
                            });
                        } else {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'Product Deleted'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Product id Not provided'
            });
        }
    };

    app.getStatsData = function (req, res) {
        Category.count().then(function (catCount) {
            Product.count().then(function (productCount) {
                return res.status(apiCodes.SUCCESS).send({
                    data: {
                        categoryCount: catCount,
                        productCount: productCount
                    }
                });
            })
                .catch(function (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting state data'});
                });
        })
            .catch(function (err) {
                return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting state data'});
            });
    };


};