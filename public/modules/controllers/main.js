'use strict';
/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the app
 */
angular.module('app')
    .controller('MainCtrl', function ($scope, $auth, $state, $http, toaster) {

        $scope.categoryCount = 0;
        $scope.productCount = 0;

        $scope.userLogout = function () {
            $auth.logout();
            $state.go('login');
        };


        $scope.stateData = function () {
            $http.get('/api/v1/stats/data').then(function (result) {
                $scope.categoryCount = result.data.data.categoryCount;
                $scope.productCount = result.data.data.productCount;
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };
        $scope.stateData();


    });
