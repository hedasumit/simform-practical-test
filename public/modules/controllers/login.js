'use strict';

angular.module('app')
    .controller('LoginCtrl', function ($scope, $auth, $state,toaster) {
        $scope.user = {};

        $scope.loginUser = function () {
            $auth.login($scope.user).then(function (user) {
                $state.go('dashboard.home');
            }).catch(function (err) {
                toaster.error(err.data.message);
                });
        }
    });
